# BlockchainProof.VerifyContentResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**registrationTime** | **Date** |  | [optional] 
**contentId** | **String** |  | 
**contentRequest** | [**ContentRequest**](ContentRequest.md) |  | 
**proofChain** | [**CommittedChain**](CommittedChain.md) |  | 
**registrationState** | **String** |  | 


<a name="RegistrationStateEnum"></a>
## Enum: RegistrationStateEnum


* `PENDING` (value: `"PENDING"`)

* `REGISTERED` (value: `"REGISTERED"`)

* `NOT_REGISTERED` (value: `"NOT_REGISTERED"`)




