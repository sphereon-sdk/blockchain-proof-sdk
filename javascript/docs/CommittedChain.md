# BlockchainProof.CommittedChain

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | Chain ID | [optional] 


