# BlockchainProof.StoredSettings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**settings** | [**Settings**](Settings.md) |  | 
**settingsChain** | [**CommittedChain**](CommittedChain.md) |  | 
**signature** | **String** |  | 
**validFrom** | **Date** |  | 


