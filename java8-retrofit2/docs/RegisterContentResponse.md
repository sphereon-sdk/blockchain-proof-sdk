
# RegisterContentResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contentRequest** | [**ContentRequest**](ContentRequest.md) |  | 
**proofChain** | [**CommittedChain**](CommittedChain.md) |  | 
**registrationState** | [**RegistrationStateEnum**](#RegistrationStateEnum) |  | 


<a name="RegistrationStateEnum"></a>
## Enum: RegistrationStateEnum
Name | Value
---- | -----
PENDING | &quot;PENDING&quot;
REGISTERED | &quot;REGISTERED&quot;
NOT_REGISTERED | &quot;NOT_REGISTERED&quot;



