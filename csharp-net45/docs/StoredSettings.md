# Sphereon.SDK.Blockchain.Proof.Model.StoredSettings
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Settings** | [**Settings**](Settings.md) |  | 
**SettingsChain** | [**CommittedChain**](CommittedChain.md) |  | 
**Signature** | **byte[]** |  | 
**ValidFrom** | **DateTime?** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

