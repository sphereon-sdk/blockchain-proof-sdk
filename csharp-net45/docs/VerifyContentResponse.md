# Sphereon.SDK.Blockchain.Proof.Model.VerifyContentResponse
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**RegistrationTime** | **DateTime?** |  | [optional] 
**ContentId** | **string** |  | 
**ContentRequest** | [**ContentRequest**](ContentRequest.md) |  | 
**ProofChain** | [**CommittedChain**](CommittedChain.md) |  | 
**RegistrationState** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

