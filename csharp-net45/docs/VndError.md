# Sphereon.SDK.Blockchain.Proof.Model.VndError
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Links** | [**Dictionary&lt;string, Link&gt;**](Link.md) |  | [optional] 
**Logref** | **string** |  | [optional] 
**Message** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

