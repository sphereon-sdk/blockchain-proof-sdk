# Sphereon.SDK.Blockchain.Proof.Model.Settings
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Version** | **int?** | The settings version (only 1 for now) | 
**HashAlgorithm** | **string** | The hashing method used (CLIENT) or to use (SERVER) for the content | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

