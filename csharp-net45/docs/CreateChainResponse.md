# Sphereon.SDK.Blockchain.Proof.Model.CreateChainResponse
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**SettingsChain** | [**CommittedChain**](CommittedChain.md) |  | 
**ChainRequest** | [**CreateChainRequest**](CreateChainRequest.md) |  | 
**ProofChain** | [**CommittedChain**](CommittedChain.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

