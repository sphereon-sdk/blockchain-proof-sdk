# Sphereon.SDK.Blockchain.Proof.Model.SettingsResponse
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**SettingsChain** | [**CommittedChain**](CommittedChain.md) |  | 
**CurrentSettings** | [**StoredSettings**](StoredSettings.md) |  | 
**AllSettings** | [**List&lt;StoredSettings&gt;**](StoredSettings.md) |  | 
**ProofChain** | [**CommittedChain**](CommittedChain.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

