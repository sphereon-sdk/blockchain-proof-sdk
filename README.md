# Blockchain Proof SDK

## Introduction

Welcome to the blockchain Proof SDK. This SDK allows you to prove existence of objects at a specific point in time, without having to worry about the low level details of blockchain or cryptocurrencies.
Actualy the notion of blockchain is hidden away for the most part in this API.

See this page on our website for more information: https://sphereon.com/products/blockchain-api

## Available SDKs

The SDK currently supports the folowing languages:
 * [Java - Jersey2](java8-jersey2)
 * [Java - Retrofit2](java8-retrofit2)
 * [C# .NET](csharp-net45)
 * [Javascript](javascript)
 
SDKs for the following languages will follow soon:
 * PHP
 * Swift3
 * Python
 * Perl
 * Android
 
Do you have the need for another language? Please contact us. In many cases we can easily accomidate you.

## Author
website: [https://sphereon.com](Sphereon.com)
e-mail: [mailto://support@sphereon.com](support@sphereon.com)

