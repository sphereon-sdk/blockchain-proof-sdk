
# StoredSettings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**settings** | [**Settings**](Settings.md) |  | 
**settingsChain** | [**CommittedChain**](CommittedChain.md) |  | 
**signature** | **byte[]** |  | 
**validFrom** | [**OffsetDateTime**](OffsetDateTime.md) |  | 



